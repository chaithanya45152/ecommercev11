import React, { useState } from "react";
import { Navigate } from "react-router-dom";
import axios from "axios";

export default function ShowLogin() {
  const [isSubmitted, setIsSubmitted] = useState(false);
  const [userLoginDetails, setUserLoginDetails] = useState({
    username: '',
    userPassword: ''
  });

  const HandleChangedValue = (event) => {
    const inputName = event.target.name;
    const inputValue = event.target.value;
    setUserLoginDetails({ ...userLoginDetails, [inputName]: inputValue });
  }

  const HandleLoginDetails = (event) => {
    event.preventDefault();
    axios.get('http://localhost:8080/SimpleLogin/UserServlet', { params: { username: userLoginDetails.username, password: userLoginDetails.userPassword } })
      .then((response) => {
        console.log(response);
        if (response.data === "Succesful") {
          alert("Login Sucess");
          setIsSubmitted(true);
        }
        else if (response.data === "Failed") {
          alert("Login Failed");
        }
        // response.data.map((obj)=>{           
        //     if (obj.username.includes(userLoginDetails.username) && obj.password.includes(userLoginDetails.userPassword)){
        //         alert("Login Sucess");           
        //     }
        //     else{
        //         alert("Login Failed");           
        //     }           
        // })
      })

  }

  return (
    <div className="card-container text-center">
      {isSubmitted ? <Navigate to="/" /> :
        <div className="form-group">
        <div>
          <form   onSubmit={HandleLoginDetails} border="10px">
            <div className="input-container" class="p-3">
              <label>Username </label>
              <input class="form-control" type="text" name="username" required onChange={HandleChangedValue} />
              {/* {renderErrorMessage("uname")} */}
            </div>
            <div className="form-group" class="p-3">
              <label>Password </label>
              <input class="form-control" type="password" name="userPassword" required onChange={HandleChangedValue} />
              {/* {renderErrorMessage("pass")} */}
            </div>
            <div className="button-container">
              <input class="btn btn-primary" type="submit" />
            </div>
          </form>
          </div>
        </div>}
    </div>
  );
}
