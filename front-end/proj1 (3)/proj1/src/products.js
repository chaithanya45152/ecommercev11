import axios from "axios";
import { useContext, useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { newContext } from "./App";
import ProdEdit from "./ProdEdit";
const Products=()=>{
    const[productdata,productchange]=useState(null);
    
    const navigate=useNavigate();
    const Loadfunction=(Pid)=> {
        navigate('/product/Edit')


    }
    const Removefunction=(Pid)=>{
        if(window.confirm('Do you want to Delete?'))
        
        //  fetch(`http://localhost:8080/ServerletJspDemo/ProductsApi${product_id}`,{
        //             method:"DELETE",
        //         }).then((res)=>{
        //           alert("removed Successfully");
        //           window.location.reload();
        //         }).catch((err)=>{
        //             console.log(err.message)
        
        //         })
        
        axios.delete(`http://localhost:8080/ProductProject/getProducts?Pid=${Pid}`)
         .then((response) => {
             console.log(response);
             })
//        
console.log(Pid);


    
}


useEffect(()=>{
    fetch("http://localhost:8080/ProductProject/getProducts")
    .then((res)=>{
      return res.json();
    }).then((resp)=>{
        productchange(resp);
    }).catch((err)=>{
        console.log(err.message);
    })
},[])
    return(
        <div className="container">
            <div className="card">
                <div className="card-title">
                    <h2>Product Details</h2>
                </div>
                <div className="card-body">
                    <div className="divbtn">
                        <Link to="product/create" className="btn btn-success">ADD New (+)</Link>
                    </div>
                    <table className="table table-bordered">
                        <thead className="bg-dark text-white">

                            <tr>
                                <td>Image</td>
                                <td>Category</td>
                                <td>Id</td>
                                <td>Name</td>
                                <td>price</td>
                                <td>Quantity</td>
                                <td>Description</td>
                                <td>Actions</td>
     
                            </tr>

                        </thead>
                        <tbody>
                            {   productdata &&
                                productdata.map(items=>(
                                    <tr key={items.Pid}>
                                        <td><img src={items.Image_url} class="img-size"/></td>
                                        <td>{items.category}</td>
                                        <td>{items.Pid}</td>
                                        <td>{items.Pname}</td>
                                        <td>{items.Pprice}</td>
                                        <td>{items.Pquantity}</td>
                                        <td>{items.Pdesc}</td>
                                        <td><a onClick={()=>{Loadfunction(items.Pid)}} className="btn btn-success">Edit</a>
                                        <a onClick={()=>{Removefunction(items.Pid)}} className="btn btn-danger">Remove</a>
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </table>

                </div>
            </div>

        </div>
    )
}
export default Products;