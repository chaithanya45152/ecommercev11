import axios from "axios";
import React, { useState } from "react";
import { Link} from "react-router-dom";
//import Products from "./products";



const ProdEdit=()=>{
    const[category,categorychange]=useState("");
    const[Pid,idchange]=useState("");
    const[Pname,prodnamechange]=useState("");
    const[Pprice,pricechange]=useState("");
    const[Pquantity,quantitychange]=useState("");
    const[Pdesc,descriptionchange]=useState("");
    const[Image_url,imagechange]=useState("");
    
        const handlesubmit=(e)=>{
            e.preventDefault();
            console.log({category,Pid,Pname,Pprice,Pquantity,Pdesc});
            
        axios.put(`http://localhost:8080/ProductProject/getProducts`,null,
        {
             params:{
                category,
                Pid,
                Pname,
                Pprice,
                Pquantity,
                Pdesc,
            Image_url}})
    
        }

    return(
        <div>
            <div className="row">
                <div className="offset-lg-3 col-lg-6">
                    <form className="container" onSubmit={handlesubmit}>
                        <div className="card" style={{"textAlign":"left"}}>
                            <div className="card-title">
                                <h2>Product Editing</h2>
                            </div>
                            <div className="card-body">
                                <div className="row">
                                <div className="col-lg-12">
                                        <div className="form-group">
                                            <label>Category</label>
                                            <input type="text" required value={category} onChange={e=>categorychange(e.target.value)}  className="form-control"/>
                                        </div>
                                    </div> 
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <label>id</label>
                                            <input type="number" required value={Pid} onChange={e=>idchange(e.target.value)}  className="form-control"/>
                                        </div>
                                    </div> 
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <label>Product Name</label>
                                            {/* <input type="number" required value={id} onChange={e=>idchange(e.target.value)}  className="form-control" hidden></input> */}
                                            <input type="text" value={Pname} required onChange={e=>prodnamechange(e.target.value)} className="form-control"></input>
                                        </div>
                                    </div>
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <label>Product Price</label>
                                            <input type="number" value={Pprice || ""} required onChange={e=>pricechange(e.target.value)}  className="form-control"></input>
                                        </div>
                                    </div>
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <label>Product Quantity</label>
                                            <input type="number" value={Pquantity || ""} required onChange={e=>quantitychange(e.target.value)}  className="form-control"></input>
                                        </div>
                                    </div>
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <label>Product Description</label>
                                            <input type="text" value={Pdesc || ""} required onChange={e=>descriptionchange(e.target.value)}  className="form-control" type="text"></input>
                                        </div>
                                    </div>
                                    {/* <div className="col-lg-12">
                                        <div className="form-group">
                                            <label>Image url</label>
                                            <input type="text" required value={Image_url} onChange={e=>imagechange(e.target.value)}  className="form-control"/>
                                        </div>
                                    </div> */}
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <button className="btn btn-success" type="submit">Save</button>
                                            <Link to="/" className="btn btn-danger">Back</Link>
                                        </div>
                                    </div> 
                                </div>

                            </div>
                        </div>

                    </form>

                </div>

            </div>

        </div>
    );
    }
export default ProdEdit;