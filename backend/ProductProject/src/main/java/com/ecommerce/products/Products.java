package com.ecommerce.products;

public class Products {
	String category;
	String Pname;
	int Pid;
	int Pprice;
	int Pquantity;
	String Pdesc;
	String Image_url;
	
	
	
	public Products(String category, String pname, int pid, int pprice, int pquantity, String pdesc, String image_url) {
		super();
		this.category = category;
		Pname = pname;
		Pid = pid;
		Pprice = pprice;
		Pquantity = pquantity;
		Pdesc = pdesc;
		Image_url = image_url;
	}
	
	
	public Products() {
		super();
	}
	
	
	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}


	public String getPname() {
		return Pname;
	}
	public void setPname(String pname) {
		Pname = pname;
	}
	public int getPid() {
		return Pid;
	}
	public void setPid(int pid) {
		Pid = pid;
	}
	public int getPprice() {
		return Pprice;
	}
	public void setPprice(int pprice) {
		Pprice = pprice;
	}
	public int getPquantity() {
		return Pquantity;
	}
	public void setPquantity(int pquantity) {
		Pquantity = pquantity;
	}
	public String getPdesc() {
		return Pdesc;
	}
	public void setPdesc(String pdesc) {
		Pdesc = pdesc;
	}
	
	public String getImage_url() {
		return Image_url;
	}

	public void setImage_url(String image_url) {
		Image_url = image_url;
	}



	
	
}