package UserWebPack;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import com.ecommerce.login.User;
//import com.ecommerce.products.Products;
//import com.ecommerce.web.dao.ProductsDao;
import com.google.gson.Gson;

import UserDaoPack.UserDao;
import UserPack.User;

/**
 * Servlet implementation class UserServlet
 */
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "*");
		response.addHeader("Access-Control-Allow-Headers", "*");
		// TODO Auto-generated method stub
//		UserDao dao=new UserDao();
		
		UserDao dao = new UserDao();
		 PrintWriter pw = response.getWriter();
		 String status="";
		 String username = request.getParameter("username");
		 String password = request.getParameter("password");
		//        String email = request.getParameter("email");
		User rec=null;
		try {
			rec =dao.login(username, password);
			if (rec != null) {
				status = "Succesful";
			}else {
				status = "Failed";}
		 }catch (Exception e) {
			e.printStackTrace();
			}
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		Gson gson = new Gson();
		String ajson = gson.toJson(status);
		pw.print(ajson);
		pw.flush();
		System.out.println(username);
		System.out.println(password);
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
